# Using library for thingspeak from https://github.com/mchwalisz/thingspeak
import thingspeak
# time is used to introduce delays in the program
import time
# We want to interface with our raspberry pi
import RPi.GPIO as GPIO
# import the GPIOzero to have it easily blink the LED's and check buttons for us
import gpiozero
# From the w1thermsensor library we import the W1ThermSensor module
from w1thermsensor import W1ThermSensor
# We import the os to easily get environment variables through the os
import os
# Use dotenv to load environment variables. Better than having a file for every fucking key or ip address etc etc
from dotenv import load_dotenv
# Since we're using the MCP3008, we'll like to use the GPIOzero library for it as well
from gpiozero import MCP3008
# Importing the mean function will make it much easier to get the average of a list instead of sum(list)/len(list)
from statistics import mean

# Load environment variables from our .env file into working memory
load_dotenv()

# Set environment variables to global variables (not best practice)
channel_id = os.getenv('channel')
write_key = os.getenv('write_key')
btn1_field = os.getenv('btn1_field')
btn2_field = os.getenv('btn2_field')
ADC_field = os.getenv('ADC_field')
temperature_field = os.getenv('temperature_field')

# Set sensor variable
sensor = W1ThermSensor()
ADC = MCP3008(channel=1)

# Set the various buttons and LEDs pins to a variable
# We're using board mode with the GPIOzero library, hence the need for the board names like so
btn1Pin = gpiozero.Button("BOARD11")
btn2Pin = gpiozero.Button("BOARD13")
VoltageLED = gpiozero.LED("BOARD15")
DataLED = gpiozero.LED("BOARD16")

# Global Variables
ADC_value = 0
btn1_value = 0
btn2_value = 0
avg_temp = []      # Needs to be a list variable for a proper average calculation
loop_duration = 15000


# Set the channel variable up
channel = thingspeak.Channel(id=channel_id, api_key=write_key)


def now_ms():                       # Find current time and return it
    ms = int(time.time() * 1000)
    return ms


loop_start = now_ms()


# Adds button presses on button 1
def readBtn1():
    if btn1Pin.is_pressed:
        global btn1_value
        btn1_value = btn1_value + 1


# Adds button presses on button 2
def readBtn2():
    if btn1Pin.is_pressed:
        global btn2_value
        btn2_value = btn2_value + 1


# If the ADC value exceed two volts, make the LED blink 3 times with each blink being 0.2 seconds long
def updateLed1():
    if ADC_value > 2:
        VoltageLED.blink(0.2, 0.8, 3)


# Blink LED2 for 0.5 seconds
def updateLed2(): #
    DataLED.blink(0.5, 0.5, 1) # The led will light for 0.5 second


# Reads the ADC value
def readADC():
    global ADC_value
    ADC_value = 3.3*ADC.value  # Use channel 1 for voltage reading


# Reads temperature and appends it to the temperature list
def readTemp():
    avg_temp.append(sensor.get_temperature())


# Preps and sends values to thingspeak
def thingspeakSend():
    channel.update({btn1_field: btn1_value, btn2_field: btn2_value, ADC_field: ADC_value, temperature_field: mean(avg_temp)})


# Zeroes all values
def resetValues():
    global btn1_value
    global btn2_value
    global ADC_value
    global avg_temp
    btn1_value = 0
    btn2_value = 0
    ADC_value = 0
    avg_temp = []


if __name__ == '__main__':
    try:
        while True:
            if now_ms() - loop_start > loop_duration:
                updateLed2()                    # We've hit the 15 second mark, blink LED2 once
                print('sending to Thingspeak')
                thingspeakSend()                # Sending data, see function for specifics
                resetValues()                   # Reset all values to their default
                loop_start = now_ms()           # Restart the loop
            else:
                readBtn1()                      # See if btn1 has been pressed. If it was, +1 to it corresponding value
                readBtn2()                      # See if btn2 has been pressed. If it was, +1 to it corresponding value
                readADC()                       # Check the ADC to see how much voltage the potentiometer is allowing
                readTemp()                      # Read current temperature and add it to the list variable
                updateLed1()                    # Read ADC value and blink if exceeding threshold
                print(f'loop elapsed ms {now_ms() - loop_start}')  # print loop time

    except:
        print("An error occurred, please check your wiring and internet connection before trying again")