from gpiozero import LED, Button

leds = LED(12, 16)
buttons = Button(18, 22)

buttons[0].when_pressed = leds[0].on
buttons[0].when_released = leds[0].off
buttons[1].when_pressed = leds[1].on
buttons[1].when_released = leds[1].off
