# Week 38 log

##Aleksis
Everything went ok except the SSH being confusing and not as easy to set up as expected.  Networking exercises went on without any issues due to well written guides. GIT can be a bit confusing at the beginning .

## Alexander
Week 38 experience: Good overall, easy to follow instructions, although the purpose of the layout was confusing until NPES and Moozer explained it to me. After that it made perfect sense. Soldering was fun and easy (thanks for the tips Daniel!). Tested the module and everything worked well.

## Dainty
Exercise ww38 and working on the lab to build our veroboard for LED and button module was actually fun, but I struggled a bit with SSH access to raspi, but with my teammates help, we manage to make it work :wink:

## Daniel
Last week exercises was good and i did almost everything. In programming i had minimal of problems that i have solved or about to. Overwise i am good.

## Jacob
I had a good experience during the week. The tasks was somewhat easy to understand, but it was hard to make the whole group reach to goal together. We're working on it though.

## Nikolaj
Setting up SSH access to my Raspberry pi took longer than it should due to a spelling error in my config. Eventually i got it working with some help from Alexander. Soldering was fun, i had done soldering in the past.

## Sheriff
Issues with week 38 exercises.  Installed vm ware workstation. Downloaded the Kali Linux software. When I try uploading Kali on the vm ware doesn�t seems to work. All I get is the blank screen. Tried it several times but still no luck.