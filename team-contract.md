# Team contract


1. Notify the team in due time if you are delayed or absent, do this before we are supposed to meet. Don't make up bullshit excuses, just tell the truth.  
2. The team will meet during school hours.  
3. Be flexible.  
4. Agreed appointments and deadlines must be met by all team members.  
5. We have confidentiality within the team and do not slander about other team members.  
6. Disagreements are handled as a group, and we shall stay open minded.  
7. If anyone has a problem with a team member, grab them and talk about it.  
8. Our meetings always have an agenda which is facilitated by one of the team members, and uploaded on g-suite 24 hours before the team meeting.  
9. Our meetings always has a secretary who writes notes.  
10. The chairperson also takes time, and uploads this to g-suite.  
11. Meeting roles are rotating.  
12. We start and end team meetings with a written update/recap, and end with an action plan.  
13. We use G-suite for minutes of meetings and shared study material.  
14. We use Discord for communicating. And Facebook messenger for backup if Discord is down.  
15. We work seriously but also allow space for fun  
16. We support each other and accept each others differences  
17. Be tolerant of people's questions and ideas.  
18. Work concentrated towards the goal and with high work ethic  
19. If people break the rules, there is a 5kr penalty, paid to a shared mobilepay box.  
20. Utilize each other's strengths and weaknesses.  
21. Keep a good tone, be civil.  
22. When online meetings are held the secretary creates the zoom meeting id, if the meeting is held on zoom.  
23. The chairperson creates the base of the next agenda, then people can add things to it under “etc.”.  
