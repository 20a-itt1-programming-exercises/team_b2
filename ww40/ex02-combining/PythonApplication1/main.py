# Using library for thingspeak from https://github.com/mchwalisz/thingspeak
import thingspeak
# time is used to introduce delays in the program
import time
# We want to interface with our raspberry pi
import RPi.GPIO as GPIO
# import LED from GPIOzero to have it easily blink the LED's
import gpiozero
# From the w1thermsensor library we import the W1ThermSensor module
from w1thermsensor import W1ThermSensor
# We import the os to easily get environment variables through the os
import os
# Use dotenv to load environment variables. Better than having a file for every fucking key or ip address etc etc
from dotenv import load_dotenv

# Load environment variables from our .env file into working memory
load_dotenv()

# Set environment variables to global variables (not best practice)
channel_id = os.getenv('channel')
write_key = os.getenv('write_key')
btn1_field = os.getenv('btn1_field')
btn2_field = os.getenv('btn2_field')
ADC_field = os.getenv('ADC_field')
temperature_field = os.getenv('temperature_field')

# Set sensor variable
sensor = W1ThermSensor()

# Set the various buttons and LEDs pins to a variable
# Tell the GPIO module that we'll be using the board layout to determine pins
btn1Pin = gpiozero.Button(11)
btn2Pin = gpiozero.Button(13)
VoltageLED = gpiozero.LED(15)         # Please notice that this specific variable is bound to the LED function from GPIOzero
DataLED = gpiozero.LED(16)

#Global Variables
ADC_value = 0
btn1_value = 0
btn2_value = 0
avg_temp = 0
loop_duration = 15000
loop_start = now_ms()

# Set the channel variable up
channel = thingspeak.Channel(id=channel_id, api_key=write_key)

# Clean up after ourselves and release any channels the GPIO pins might be reserved for
def destroy():
    GPIO.cleanup()

def now_ms():
    ms = int(time.time() * 1000)
    return ms

def readBtn1(btn1_value):   # Counts button presses on button 1 (sheriff, Daniel)

def readBtn2(btn2_value):   # Counts button presses on button 2 (Sheriff, Daniel)

def updateLed1(VoltageLED): # (Sheriff, Daniel)

def updateLed2(DataLED): # (Dainty)

def readADC(ADC_value): # Reads the ADC value (Jacob)

def readTemp(avg_temp): # Reads temperature and calculates the average (nikolaj)

def thingspeakSend(btn1_value, btn2_value, ADC_value, avg_temp):    # Sends values to thingspeak
    response = channel.update({btn1_field: btn1_value, btn2_field: btn2_value, ADC_field: ADC_value, temperature_field: avg_temp})

def resetValues(btn1_value, btn2_value, ADC_value, avg_temp):   # Resets values (Dainty)

# Measure temperature, then return true/false in case of errors
def measureTemperature():
    try:
        print('Measuring!')
        current_temp = sensor.get_temperature()         # Collect current temperature and save it to a variable
        response = temperature_channel.update({temperature_field: current_temp})    # Send current temp to thingspeak
        print(f'It is currently {current_temp} degrees, sent to thingspeak')
        if response == 0:       # Response will only be 0 if one has already reached their quota limit
            return False
        else:
            return True
    except:
        print('A networking error has occured, flashing connection light...')
        update_status(False)

if __name__ == '__main__': # (alexander)