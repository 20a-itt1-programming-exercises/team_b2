###Week 41 Progress
Today, monday the 19th of October 2020, we dedicated our day, and worked on the Project.

The day was compositioned of three mayor things:

1) Self-assessment tests
2) ADC Readings
3) Fablab introductions

*Note: A lot of team members were not present, due to illness.*
**first:**
The self-assessment test were answered (like last time in week 41) for those present. We had to answer one in the morning, and one in the afternoon, to keep track of our progress and what we have learned.

**Second:**
The second part was about learning how to read a voltage output, using a ADC module, and an IC; the MCP3008.
We were provided with [documentation for the module](https://eal-itt.gitlab.io/discover-iot/rpi/adc), and given a basic understanding of it.
We were also provided with a small potentiometer for the purpose, of manipulating the output value.

We then wrote some code, base off of the [documentation for the GPIO zero libeary.](https://gpiozero.readthedocs.io/en/stable/recipes.html#potentiometer):
>from gpiozero import MCP3008
>
>pot = MCP3008(channel=0)
>
>while True:
>    print("Current Voltage: " + str(pot.value*3.3)+ "V")

Here, we display the value in voltage to the terminal.

**Thrid:**
As part of the educational plan, we were introduced to the shcool's Fablab. For those of us present, we tried out the laser cutting machine, and after some introductions to the machine, and general knowledge about the whole fablab, we recieved a *"LaserCutting Certification Badge"*, which can be used in many ways.
For those not present, they will be tagged so they can see this, and schedule a time to visit the fablab later on.