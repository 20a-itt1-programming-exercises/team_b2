# Summary

*Summary of the issue*

# What needs to be done

*A checklist of subtasks*

- [ ] 
- [ ] 
- [ ] 
- [ ] 

# Output

*what we expect to happen/get out of it"

## Responsible

*Who is responsible for fixing this issue*

If the issue requires work from others please do not assign attendees or put the issue in the doing column before it has been agreed upon on a team meeting.

## Link to documentation related to the task

*Any and all relevant material*