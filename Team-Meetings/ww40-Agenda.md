# Team morning meeting agenda :


##### 1. Round the table: What did I do, and what did I finish?

- Aleksis- programming exercises, struggling with ssh related tasks
- Alexander- completed led module test, no issues
- Dainty- thinkspeak finish, not done with led
- Daniel- practice freenove exercises, learning coding
- Jacob- finish all project exercises, confuse about the general overview
- Nikolaj- trying to fix ssh agent, need to fix merge request
- Sheriff- will connect led to rpi, and finish the rest of tasks


##### 2. Review of tasks: Are they still relevant? do we need to add new ones?

1. Create log of ww38 - Each should write short experience
2. Team contract 
3. Read-'how stuff works'
4. Pair up and go to the lab
5. Connect to thinkspeak
6. LED module test
7. SSH keys agent forwarding
8. Contributing code on gitlab

##### 3.Round the table: Claim one task each.

- POC Video - nikolaj
- DS18B20 temperature sensor -sheriff
- Combining parts - alexander

##### 4. Any other business (AOB)

-n/a

# Teacher meeting agenda :

1. Status on project (ie. show closed tasks in gitlab)
Finish ww39 so we do the upcoming exercises in week 40

2. Next steps (ie. show next tasks in gitlab)

3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
collaboration is solid!

4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)